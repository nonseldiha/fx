(ns nonseldiha.fx-test
  (:require [clojure.test :as t]
            [nonseldiha.fx :as fx :refer [fx]]))

(t/deftest fx-identity-is-noop
  (fx/with-fx fx/identity
    (t/is (= 3 (fx (+ 1 2))))))

(t/deftest fx-time-returns-time
  (fx/with-fx fx/time
    (t/is (< (fx (+ 1 2)) 10000))))

(t/deftest fx-form-returns-the-form
  (fx/with-fx fx/form
    (t/is (= '(+ 1 2) (fx (+ 1 2))))))

(comment
  (time (dotimes [_ 1000000] (+ 1 2)))
  (time (dotimes [_ 1000000] (fx/with-fx fx/time (+ 1 2)))))
