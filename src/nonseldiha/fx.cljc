(ns nonseldiha.fx
  (:refer-clojure :exclude [identity time]))

(def fx-handler nil)
(defmacro fx [form]
  `(if fx-handler
     (let [tstart# (System/nanoTime)
           v# ~form
           t# (unchecked-subtract (System/nanoTime) tstart#)]
       (fx-handler {::form '~form ::took-ns t# ::v v#}))
     ~form))

(defmacro with-fx [fx-handler & body] `(with-redefs [fx-handler ~fx-handler] ~@body))
(defn identity [o] (::v o))
(defn time [o] (::took-ns o))
(defn form [o] (::form o))

;; tracing
;; TODO use TLS
(def ^:dynamic *trace-parent*)
